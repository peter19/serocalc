#include <math.h>
#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp;
#define NSUB 7
#define STACKSIZE 50
#define EPS1 1.0e-3
#define EPS2 1.0e-8
#define MAXLOGY 13.8

/* Remove extreme values from a sorted vector xx of length nx */
void reminf(double *xx, int nx)
{
  int k,j = nx - 1;
  double xxm, xxd;
  while(xx[j] > MAXLOGY) j = j - 1; /* y > 1e6 */
  xxm = xx[j-1];
  xxd = (xxm - xx[0])/100;
  for(k = 1; k<=nx-1-j; k++) xx[k+j-1]=xxm+k*xxd;
}

/* Given an array obs[1..n1], containing observed data and an array       */
/* sim[1..n2] with simulated data this function returns the A-D           */
/* statistic a. The arrays obs and sim must be sorted into ascending order*/
double adstattwo(double *obs, int nobs, double *sim, int nsim)
{
  int jobs=1, jsim=1;
  double enobs, ensim, *u, sm=0;
  enobs = (double) nobs;
  ensim = (double) nsim;
  u =(double *)malloc((size_t) ((nobs+1)*sizeof(double)));
  while(jobs<=nobs){                             /* while we are not done */
    if(obs[jobs-1] >  sim[jsim-1] && jsim < nsim) jsim++;  /* step in sim */
    if(obs[jobs-1] <= sim[jsim-1]){
      u[jobs]=(jsim-1)/ensim;
      if(jsim==1) u[jobs]=1/(2*ensim);
      jobs++;                                              /* step in obs */
    }
    if(jsim==nsim && jobs <= nobs){
      u[jobs]=(ensim-0.5)/ensim;
      jobs++;                                              /* step in obs */
    }
  }
  for(jobs=1;jobs<=nobs;jobs++){
    sm = sm+(2*jobs-1)*(log(u[jobs])+log(1-u[nobs-jobs+1]));
  }
  free((char *) (u));
  return -enobs-sm/enobs;
}

/* Given an array data1[1..n1], and an array data2[1..n2] this function   */
/* returns an estimate of the two-sample Kullback-Leibler divergence d.   */
/* The arrays data1 and data2 must be sorted into ascending order.        */
double kldivtwo(double *xs, int nx, double *ys, int ny)
{
  int k=1, m=1;
  double logp, logq, ylo, enx, eny, lognx, logny, log2 = log(2);
  reminf(xs,nx);
  reminf(ys,ny);
  enx = (double) nx; lognx = log(enx);
  eny = (double) ny; logny = log(eny);
  logq = 0;
  while(k <= nx){
    while(ys[m-1] > xs[k-1] && k <= nx){
      ylo = xs[0]; if(m > 1) ylo = ys[m-2];
      logq = logq - logny - log(ys[m-1]-ylo);
      k = k + 1;
    }
    if(ys[m-1] <= xs[k-1] && k <= nx){
      if(m==ny){
	logq = logq - logny - log(xs[nx-1]-ys[m-1]);
	k = k + 1;
      }
      if(m < ny) m = m + 1;
    }
  }
  logp = -lognx - log(xs[1]-xs[0]);
  for(k = 2; k <= nx-1; k++)
    logp = logp - lognx + log(1/(xs[k-1]-xs[k-2]) + 1/(xs[k]-xs[k-1])) - log2;
  logp = logp - lognx - log(xs[nx-1]-xs[nx-2]);
  return (logp - logq)/enx - 1;
}

/* Given an array data1[1..n1], and an array data2[1..n2] this function   */
/* returns the Kolmogorov-Smirnov statistic d. The arrays data1 and data2 */
/* must be sorted into ascending order.                                   */
double ksdevtwo(double *data1, int n1, double *data2, int n2)
{
  int j1=1, j2=1;
  double d, d1, d2, dt, en1, en2, fn1=0.0, fn2=0.0;
  en1 = (double) n1;
  en2 = (double) n2;
  d = 0.0;
  while(j1<=n1 && j2<=n2){ /* Wile we are not done */
    if((d1=data1[j1-1]) <= (d2=data2[j2-1])) fn1=j1++/en1; /* step in data1 */
    if(d2<=d1) fn2=j2++/en2;                               /* step in data2 */
    if((dt=fabs(fn2-fn1)) > d) d = dt;
  }
  return d;
}

/* Given an array data1[1..n1], and an array data2[1..n2] this function   */
/* returns the Kuiper statistic d. The arrays data1 and data2 must be     */
/* sorted into ascending order.                                           */
double kpdevtwo(double *data1, int n1, double *data2, int n2)
{
  int j1=1, j2=1;
  double dplus, dmin, d1, d2, dt, en1, en2, fn1=0.0, fn2=0.0;
  en1 = (double) n1;
  en2 = (double) n2;
  dplus = 0.0;
  dmin = 0.0;
  while(j1<=n1 && j2<=n2){ /* Wile we are not done */
    if((d1=data1[j1-1]) <= (d2=data2[j2-1])) fn1=j1++/en1; /* step in data1 */
    if(d2<=d1) fn2=j2++/en2;                               /* step in data2 */
    if((dt=(fn2-fn1)) > dplus) dplus = dt;
    if((dt=(fn1-fn2)) > dmin) dmin = dt;
  }
  return dplus + dmin;
}

/* Given an array data1[1..n1], and an array data2[1..n2] this function   */
/* returns the Kolmogorov probability for the null hypothesis that the    */
/* data sets are drawn from the same distribution.                        */
double ksprobtwo(double *data1, int n1, double *data2, int n2)
{
  int j1=1, j2=1;
  double prob, d, d1, d2, dt, en1, en2, en, fn1=0.0, fn2=0.0;
  en1 = (double) n1;
  en2 = (double) n2;
  d = 0.0;
  while(j1<=n1 && j2<=n2){ /* Wile we are not done */
    if((d1=data1[j1-1]) <= (d2=data2[j2-1])) fn1=j1++/en1; /* step in data1 */
    if(d2<=d1) fn2=j2++/en2;                               /* step in data2 */
    if((dt=fabs(fn2-fn1)) > d) d = dt;
  }
  en = sqrt(en1*en2/(en1+en2));
  prob = probks((en+0.12+0.11/en)*d); /* Compute probability */
  return prob;
}

/* Kolmogorov probability function                                */
double probks(double alam)
{
  int j;
  double a2, fac=2.0, sum=0.0, term, termbf=0.0;
  a2 = -2.0*alam*alam;
  for(j=1; j<100; j++){
    term = fac*exp(a2*j*j);
    sum += term;
    if(fabs(term) <= EPS1*termbf || fabs(term) <= EPS2*sum) return sum;
    fac = -fac; /* Alternating signs in sum. */
    termbf = fabs(term);
  }
  return 1.0; /* convergence failure. */
}

/* Sorts an array arr[0..n-1] into ascending numerical order using */
/* Quicksort. NSUB is the size of subarrays sorted by straight     */
/* insertion. STACKSIZE is the size of required auxiliary storage. */
void sort(double *arr, int n)
{
  int i, ir=n, j, k, l=1;
  int jstack=0, *istack;
  double a, temp;
  istack=(int *)malloc((size_t) ((STACKSIZE+1)*sizeof(int)));
  for(;;){            /* Insertion sort when subarray small enough */
    if(ir-l<NSUB){
      for(j=l+1;j<=ir;j++){
	a=arr[j-1];
	for(i=j-1;i>=1;i--){
	  if(arr[i-1]<=a) break;
	  arr[i]=arr[i-1];
	}
	arr[i]=a;
      }
      if(jstack==0) break;
      ir=istack[jstack--];        /* Pop stack and begin a new round      */
      l=istack[jstack--];         /* of partitioning                      */
    }else{
      k=(l+ir)>>1;                /* Choose median of left, center, and   */
      SWAP(arr[k-1],arr[l]);      /* right elements as partitioning       */
      if(arr[l]>arr[ir-1]){       /* element a. Also rearrange so that    */
	SWAP(arr[l],arr[ir-1]);   /* a[l+1] <= a[l] <= a[ir].             */
      }
      if(arr[l-1]>arr[ir-1]){
	SWAP(arr[l-1],arr[ir-1]);
      }
      if(arr[l]>arr[l-1]){
	SWAP(arr[l],arr[l-1]);
      }
      i=l+1;                      /* Initialize pointers for partitioning */
      j=ir;
      a=arr[l-1];                 /* Partitioning element                 */
      for(;;){                    /* Beginning of innermost loop          */
	do i++; while(arr[i-1]<a);/* Scan up to find element > a          */
	do j--; while(arr[j-1]>a);/* Scan down to find element < a        */
	if(j<i) break;            /* Pointers crossed. Partitioning done  */
	SWAP(arr[i-1],arr[j-1]);  /* Exchange elements                    */
      }                           /* End of innermost loop                */
      arr[l-1]=arr[j-1];          /* Insert partitioning element          */
      arr[j-1]=a;
      jstack+=2;
      if(jstack>STACKSIZE){
	fprintf(stderr,"STACKSIZE too small in sort.\n");
	exit(1);
      }
      if(ir-i+1>=j-l){
	istack[jstack]=ir;
	istack[jstack-1]=i;
	ir=j-1;
      }else{
	istack[jstack]=j-1;
	istack[jstack-1]=l;
	l=i;
      }
    }
  }
  free((char *) (istack));
}
