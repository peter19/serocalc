/* This software is released under the GNU AGPL v3.0 or later */
/* see agpl-3.0.txt */

/* Function definitions */
double afn (double *iv, double *pv);
double bfn (double *pv);
double t1fn (double *iv, double *pv);
double y1fn (double *iv, double *pv);
double ag (double  t, double *iv, double *pv);
double ab (double  t, double *iv, double *pv);
int symp (double *iv, double *pv);
double tinterv (double lambda);
int rndind (int veclen);
double rndunif (double *basel);
double rndlnorm (double *basel);

double ymemry (double age, double lambda,
               double *iv, double *pv, double *bsl);
double vinterv (double ainf, double avacc, double lambda);
double yvmemry (double age, double avacc, double lambda,
                double *iv, double *pv, double *bsl);
double yvrenew (double age, double avacc, double lambda,
                double *iv, double *pv, double *bsl);
double yintervmemry (double age, double lambda1, double tau_interv,
		     double lambda2, double *iv, double *pv, double *bsl);
double ytaumemry (double age, int ninf, double *tau,
                  double *iv, double *pv, double *bsl);

double drndksa_memry (double *logyobs, int nobs, int nsim, double *age,
		      double avacc, double lambda, int nmc, double *y0,
		      double *b0, double *mu0, double *mu1, double *c,
		      double *alpha, double *shape, double *bsl);
double drndksap_memry (double *logyobs, int nobs, int nsim, double *age,
                       double avacc,double lambda, int nmc, double *y0,
		       double *b0, double *mu0, double *mu1, double *c,
		       double *alpha, double *shape, double *bsl);
double drndksapinterv (double *logyobs, int nobs, int nsim, double *age,
                       double lambda1, double tau_interv, double lambda2,
                       int nmc, double *y0, double *b0,
                       double *mu0, double *mu1, double *c, double *alpha,
                       double *shape, double *bsl);
double drndklda_memry (double *logyobs, int nobs, int nsim, double *age,
		       double avacc, double lambda, int nmc, double *y0,
		       double *b0, double *mu0, double *mu1, double *c,
		       double *alpha, double *shape, double *bsl);
double drndada_memry (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl);
double drndksa_renew (double *logyobs, int nobs, int nsim, double *age,
		      double avacc, double lambda, int nmc, double *y0,
		      double *b0, double *mu0, double *mu1, double *c,
		      double *alpha, double *shape, double *bsl);
double drndksap_renew (double *logyobs, int nobs, int nsim, double *age,
                       double avacc,double lambda, int nmc, double *y0,
		       double *b0, double *mu0, double *mu1, double *c,
		       double *alpha, double *shape, double *bsl);
double drndklda_renew (double *logyobs, int nobs, int nsim, double *age,
		       double avacc, double lambda, int nmc, double *y0,
		       double *b0, double *mu0, double *mu1, double *c,
		       double *alpha, double *shape, double *bsl);
double drndada_renew (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl);

double drndksa_swap (double *logyobs, int nobs, int nsim, double *age,
		     double avacc, double lambda, int nmc, double *y0,
		     double *b0, double *mu0, double *mu1, double *c,
		     double *alpha, double *shape, double *bsl);
double drndksap_swap (double *logyobs, int nobs, int nsim, double *age,
                      double avacc,double lambda, int nmc, double *y0,
		      double *b0, double *mu0, double *mu1, double *c,
		      double *alpha, double *shape, double *bsl);
double drndklda_swap (double *logyobs, int nobs, int nsim, double *age,
		      double avacc, double lambda, int nmc, double *y0,
		      double *b0, double *mu0, double *mu1, double *c,
		      double *alpha, double *shape, double *bsl);
double drndada_swap (double *logyobs, int nobs, int nsim, double *age,
                     double avacc, double lambda, int nmc, double *y0,
                     double *b0, double *mu0, double *mu1, double *c,
                     double *alpha, double *shape, double *bsl);

void sort(double *arr, int n);
double probks(double alam);
double kldivtwo(double *xs, int nx, double *ys, int ny);
void reminf(double *xx, int nx);
double ksdevtwo(double *data1, int n1, double *data2, int n2);
double ksprobtwo(double *data1, int n1, double *data2, int n2);
double kpdevtwo(double *data1, int n1, double *data2, int n2);
double adstattwo(double *obs, int nobs, double *sim, int nsim);

/* Utility functions for calls from R */
void ar (double *res, double *iv, double *pv){
  *res = afn(iv,pv);
}
void br (double *res, double *pv){
  *res = bfn(pv);
}
void t1r (double *res, double *iv, double *pv){
  *res = t1fn(iv,pv);
}
void y1r (double *res, double *iv, double *pv){
  *res = y1fn(iv,pv);
}
void agr (double *res, double *t, double *iv, double *pv){
  *res = ag(*t,iv,pv);
}
void abr (double *res, double *t, double *iv, double *pv){
  *res = ab(*t,iv,pv);
}
void sympr (int *res, double *iv, double *pv){
  *res = symp(iv,pv);
}
void tintervr (double *res, double *lambda){
  *res = tinterv(*lambda);
}
void rndindr (int *res, int *veclen){
  *res = rndind(*veclen);
}
void rndunifr (double *res, double *basel){
  *res = rndunif(basel);
}
void rndlnormr (double *res, double *basel){
  *res = rndlnorm(basel);
}

void ymemryr (double *res, double *age, double *lambda,
              double *iv, double *pv, double *bsl){
  *res = ymemry(*age,*lambda,iv,pv,bsl);
}
void vintervr (double *res, double *ainf, double *avacc, double *lambda){
  *res = vinterv(*ainf,*avacc,*lambda);
}
void yvmemryr (double *res, double *age, double *avacc, double *lambda,
               double *iv, double *pv, double *bsl){
  *res = yvmemry(*age,*avacc,*lambda,iv,pv,bsl);
}
void yvrenewr (double *res, double *age, double *avacc, double *lambda,
               double *iv, double *pv, double *bsl){
  *res = yvrenew(*age,*avacc,*lambda,iv,pv,bsl);
}
void yintervmemryr (double *res, double *age, double *lambda1,
                    double *tau_interv, double *lambda2,
                    double *iv, double *pv, double *bsl){
  *res = yintervmemry(*age,*lambda1,*tau_interv,*lambda2,iv,pv,bsl);
}
void ytaumemryr (double *res, double *age, int *ninf, double *tau,
                 double *iv, double *pv, double *bsl){
  *res = ytaumemry(*age,*ninf,tau,iv,pv,bsl);
}

void drndksamemryr (double *res, double *logyobs, int *nobs,
                    int *nsim, double *age, double *avacc,
                    double *lambda, int *nmc, double *y0, double *b0,
                    double *mu0, double *mu1, double *c, double *alpha,
                    double *shape, double *bsl){
  *res = drndksa_memry (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                        *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndksapmemryr (double *res, double *logyobs, int *nobs,
                     int *nsim, double *age, double *avacc,
                     double *lambda, int *nmc, double *y0, double *b0,
                     double *mu0, double *mu1, double *c, double *alpha,
                     double *shape, double *bsl){
  *res = drndksap_memry (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                         *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndksapintervr (double *res, double *logyobs, int *nobs,
                      int *nsim, double *age, double *lambda1,
                      double *tau_interv,
                      double *lambda2, int *nmc, double *y0, double *b0,
                      double *mu0, double *mu1, double *c, double *alpha,
                      double *shape, double *bsl){
  *res = drndksapinterv (logyobs,*nobs,*nsim,age,*lambda1,*tau_interv,*lambda2,
                         *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndkldamemryr (double *res, double *logyobs, int *nobs,
                     int *nsim, double *age, double *avacc,
                     double *lambda, int *nmc, double *y0, double *b0,
                     double *mu0, double *mu1, double *c, double *alpha,
                     double *shape, double *bsl){
  *res = drndklda_memry (logyobs,*nobs,*nsim,age,*avacc,*lambda,
		         *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndadamemryr (double *res, double *logyobs, int *nobs,
                    int *nsim, double *age, double *avacc,
                    double *lambda, int *nmc, double *y0, double *b0,
                    double *mu0, double *mu1, double *c, double *alpha,
                    double *shape, double *bsl){
  *res = drndada_memry (logyobs,*nobs,*nsim,age,*avacc,*lambda,
		        *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}

void drndksarenewr (double *res, double *logyobs, int *nobs,
                    int *nsim, double *age, double *avacc,
                    double *lambda, int *nmc, double *y0, double *b0,
                    double *mu0, double *mu1, double *c, double *alpha,
                    double *shape, double *bsl){
  *res = drndksa_renew (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                        *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndksaprenewr (double *res, double *logyobs, int *nobs,
                     int *nsim, double *age, double *avacc,
                     double *lambda, int *nmc, double *y0, double *b0,
                     double *mu0, double *mu1, double *c, double *alpha,
                     double *shape, double *bsl){
  *res = drndksap_renew (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                         *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndkldarenewr (double *res, double *logyobs, int *nobs,
                     int *nsim, double *age, double *avacc,
                     double *lambda, int *nmc, double *y0, double *b0,
                     double *mu0, double *mu1, double *c, double *alpha,
                     double *shape, double *bsl){
  *res = drndklda_renew (logyobs,*nobs,*nsim,age,*avacc,*lambda,
		         *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndadarenewr (double *res, double *logyobs, int *nobs,
                    int *nsim, double *age, double *avacc,
                    double *lambda, int *nmc, double *y0, double *b0,
                    double *mu0, double *mu1, double *c, double *alpha,
                    double *shape, double *bsl){
  *res = drndada_renew (logyobs,*nobs,*nsim,age,*avacc,*lambda,
		        *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}

void drndksaswapr (double *res, double *logyobs, int *nobs,
                   int *nsim, double *age, double *avacc,
                   double *lambda, int *nmc, double *y0, double *b0,
                   double *mu0, double *mu1, double *c, double *alpha,
                   double *shape, double *bsl){
  *res = drndksa_swap (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                       *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndksapswapr (double *res, double *logyobs, int *nobs,
                    int *nsim, double *age, double *avacc,
                    double *lambda, int *nmc, double *y0, double *b0,
                    double *mu0, double *mu1, double *c, double *alpha,
                    double *shape, double *bsl){
  *res = drndksap_swap (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                        *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}
void drndkldaswapr (double *res, double *logyobs, int *nobs,
                    int *nsim, double *age, double *avacc,
                    double *lambda, int *nmc, double *y0, double *b0,
                    double *mu0, double *mu1, double *c, double *alpha,
                    double *shape, double *bsl){
  *res = drndklda_swap (logyobs,*nobs,*nsim,age,*avacc,*lambda,
		        *nmc, y0, b0, mu0, mu1, c, alpha, shape,bsl);
}

void kldivtwor (double *res, double *xs, int *nx, double *ys, int *ny){
  *res = kldivtwo(xs,*nx,ys,*ny);
}

void ksdevtwor (double *res, double *data1, int *n1, double *data2, int *n2){
  *res = ksdevtwo(data1,*n1,data2,*n2);
}

void ksprobtwor (double *res, double *data1, int *n1, double *data2, int *n2){
  *res = ksprobtwo(data1,*n1,data2,*n2);
}
