/* This software is released under the GNU AGPL v3.0 or later */
/* see agpl-3.0.txt */

/* Function definitions */
double yvgener (double age, double avacc, double lambda,
                double *iv, double *pv, double *bsl, int ginf);
double ytaugener (double age, int ninf, double *tau,
                  double *iv, double *pv, double *bsl, int ginf);

double drndksa_gener (double *logyobs, int nobs, int nsim, double *age,
		      double avacc, double lambda, int nmc, double *y0,
		      double *b0, double *mu0, double *mu1, double *c,
		      double *alpha, double *shape, double *bsl, int ginf);
double drndksap_gener (double *logyobs, int nobs, int nsim, double *age,
                       double avacc,double lambda, int nmc, double *y0,
		       double *b0, double *mu0, double *mu1, double *c,
		       double *alpha, double *shape, double *bsl, int ginf);

/* Utility functions for calls from R */
void yvgenerr (double *res, double *age, double *avacc, double *lambda,
               double *iv, double *pv, double *bsl, int *ginf){
  *res = yvgener(*age,*avacc,*lambda,iv,pv,bsl,*ginf);
}
void ytaugenerr (double *res, double *age, int *ninf, double *tau,
                 double *iv, double *pv, double *bsl, int *ginf){
  *res = ytaugener(*age,*ninf,tau,iv,pv,bsl,*ginf);
}

void drndksagenerr (double *res, double *logyobs, int *nobs,
                    int *nsim, double *age, double *avacc,
                    double *lambda, int *nmc, double *y0, double *b0,
                    double *mu0, double *mu1, double *c, double *alpha,
                    double *shape, double *bsl, int *ginf){
  *res = drndksa_gener (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                        *nmc,y0,b0,mu0,mu1,c,alpha,shape,bsl,*ginf);
}
void drndksapgenerr (double *res, double *logyobs, int *nobs,
                     int *nsim, double *age, double *avacc,
                     double *lambda, int *nmc, double *y0, double *b0,
                     double *mu0, double *mu1, double *c, double *alpha,
                     double *shape, double *bsl, int *ginf){
  *res = drndksap_gener (logyobs,*nobs,*nsim,age,*avacc,*lambda,
                         *nmc,y0,b0,mu0,mu1,c,alpha,shape,bsl,*ginf);
}
