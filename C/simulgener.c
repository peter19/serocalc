/* This software is released under the GNU AGPL v3.0 or later */
/* see agpl-3.0.txt */

#include <R.h>
#include <Rinternals.h>
#include <Rmath.h>
#include "simulgener.h"

#define MAXINF 25 /* maximum nr past infections accounted for */

/* Modified routine for calculating antibody levels at age of sampling,     */
/* Poisson process infections with rate lambda and vaccination at age avacc */
/* iv and pv and bsl are initial values (y0, b0) and kinetic parameters     */
/* (mu0, mu1, c1, alpha, r) and baseline (lognormal) parameters.            */
/* assuming ginf subsequent generations of infections with specified pv     */
double yvgener (double age, double avacc, double lambda,
                double *iv, double *pv, double *bsl, int ginf){
  double tau, arst, ainf, basl[2], ynoise, kv[5];
  int kp,minf;
  arst = age; ainf = 0;
  basl[0] = iv[0]; basl[1] = iv[1];
  ynoise = rndlnorm(bsl); /* need this only once */
  tau = vinterv(ainf,avacc,lambda); /* age at first infection */
  if(age < tau) return ynoise; /* no infection */
  minf = 1; /* start with first infection episode */
  if(age*lambda > MAXINF){
    tau = age - MAXINF/lambda; /* time for < MAXINF infections */
    minf = ginf; /* start with ginf-th infection episode */
  }
  arst = age - tau; /* remaining time */
  ainf = ainf + tau;
  tau = vinterv(ainf,avacc,lambda); /* time to next infection */
  for(kp = 0; kp < 5; kp++) kv[kp] = pv[kp + (minf-1)*5];
  if(arst < tau) return ab(arst,basl,kv)+ynoise; /* first inf */
  while(arst > tau){ /* is there time for another infection?*/
    arst = arst - tau; /* remaining time after current infection */
    ainf = ainf + tau; /* age at current infection */
    basl[0] = ab(tau,basl,kv); /* baseline for current infection */
    tau = vinterv(ainf,avacc,lambda); /* time to next infection */
    minf =  minf + 1; /* yes: there has been another infection */
    if(minf > ginf) minf = ginf; /* maximum nr inf included in long mod */
    for(kp = 0; kp < 5; kp++) kv[kp] = pv[kp + (minf-1)*5];
  }
  return ab(arst,basl,kv)+ynoise;
}
/* Generate antibody level for a person sampled at a given age   */
/* Infection history is in tau[0],tau[1],tau[2],...,tau[ninf-1]] */
double ytaugener (double age, int ninf, double *tau,
                  double *iv, double *pv, double *bsl, int ginf){
  int kp, kinf = 1, minf = 1;
  double basl[2], ynoise, kv[5];
  for(kp = 0; kp < 5; kp++) kv[kp] = pv[kp + (minf-1)*5];
  basl[0] = iv[0]; basl[1] = iv[1];
  ynoise = rndlnorm(bsl); /* need this only once */
  if(ninf == 0) return(ynoise); /* no infection */
  if(ninf == 1) return(ab(age-tau[0],basl,kv)+ynoise); /* one infection */
  while(kinf < ninf){
    basl[0] = ab(tau[kinf]-tau[kinf-1],basl,kv); /* basl current inf */
    kinf = kinf + 1;
    minf = minf + 1;
    if(minf > ginf) minf = ginf; /* maximum nr inf included in longmod */
    for(kp = 0; kp < 5; kp++) kv[kp] = pv[kp + (minf-1)*5];
  }
  return(ab(age-tau[kinf-1],basl,kv)+ynoise);
}

/* From here deviate metrics for repeated infections in       */
/* subjects sampled at a given age with carried over y0       */
/* Note that yobs must be sorted!! (and ages accordingly)     */
/* Kolmogorov-Smirnow deviate                                 */
double drndksa_gener (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl, int ginf){
  int ninf, kmc, ksim, kgen;
  double dev, ivmc[2], pvmc[ginf*5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    for(kgen=0; kgen < ginf; kgen++){
      pvmc[0+kgen*5] = mu0[kmc*ginf+kgen];
      pvmc[1+kgen*5] = mu1[kmc*ginf+kgen];
      pvmc[2+kgen*5] = c[kmc*ginf+kgen];
      pvmc[3+kgen*5] = alpha[kmc*ginf+kgen];
      pvmc[4+kgen*5] = shape[kmc*ginf+kgen];
    }
    logysim[ksim] = log(yvgener(age[ksim],avacc,lambda,ivmc,pvmc,bsl,ginf));
  }
  sort(logysim,nsim);
  dev = ksdevtwo(logyobs,nobs,logysim,nsim);
  return dev;
}

/* Kolmogorov-Smirnow probability                             */
double drndksap_gener (double *logyobs, int nobs, int nsim, double *age,
                       double avacc, double lambda, int nmc, double *y0,
                       double *b0, double *mu0, double *mu1, double *c,
                       double *alpha, double *shape, double *bsl, int ginf){
  int ninf, kmc, ksim, kgen;
  double prb, ivmc[2], pvmc[ginf*5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    for(kgen=0; kgen < ginf; kgen++){
      pvmc[0+kgen*5] = mu0[kmc*ginf+kgen];
      pvmc[1+kgen*5] = mu1[kmc*ginf+kgen];
      pvmc[2+kgen*5] = c[kmc*ginf+kgen];
      pvmc[3+kgen*5] = alpha[kmc*ginf+kgen];
      pvmc[4+kgen*5] = shape[kmc*ginf+kgen];
    }
    logysim[ksim] = log(yvgener(age[ksim],avacc,lambda,ivmc,pvmc,bsl,ginf));
  }
  sort(logysim,nsim);
  prb = ksprobtwo(logyobs,nobs,logysim,nsim);
  return prb;
}
