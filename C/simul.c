/* This software is released under the GNU AGPL v3.0 or later */
/* see agpl-3.0.txt */

/* compile with R CMD SHLIB simul.c */
#include <R.h>
#include <Rinternals.h>
#include <Rmath.h>
#include "simul.h"
#include "edfstat.c"
#include "simulgener.c"

#define MAXINF 25 /* maximum nr past infections accounted for */

/* parameter vector pv: (mu0,mu1,c1,alpha,shape) */
/* initials vector iv: (y0,b0) */
double afn (double *iv, double *pv){
  return (pv[1]-pv[0])/(pv[2]*iv[0]);
}
double bfn (double *pv){
  return pv[1]/(pv[1]-pv[0]);
}
double t1fn (double *iv, double *pv){
  return log(1+afn(iv,pv)*iv[1])/(pv[1]-pv[0]);
}
double y1fn (double *iv, double *pv){
  return iv[0]*pow((1+afn(iv,pv)*iv[1]),bfn(pv));
}
/* pathogen  kinetics: a(nti)g(en) response */
double ag (double t, double *iv, double *pv){
  double bt = 0;
  if(t > t1fn(iv,pv)) return bt;
  bt = iv[1]*exp(pv[0]*t)-pv[1]*iv[0]*(exp(pv[0]*t)-exp(pv[1]*t))/(pv[0]-pv[1]);
  return bt;
}
/* a(nti)b(ody) kinetics: power function decay */
double ab (double t, double *iv, double *pv){
  double yt;
  double t1 = t1fn(iv,pv);
  double y1 = y1fn(iv,pv);
  if(t <= t1) yt = iv[0]*exp(pv[1]*t);
  if(t >  t1) yt = pow(pow(y1,1-pv[4])-(1-pv[4])*pv[3]*(t - t1),1/(1-pv[4]));
  return yt;
}
/* symptomatic or asymptomatic seroconversion? */
int symp (double *iv, double *pv){
  int smp = 0;
  double ymin = pv[0]*iv[1]/pv[2];
  if(ymin >= iv[0]) smp = 1;
  return smp;
}
/* generate Poisson random interval */
double tinterv (double lambda){
  double u;
  GetRNGstate();
  u = -log(runif(0.0,1.0))/lambda;
  PutRNGstate();
  return u;
}
/* generate random index */
int rndind (int veclen){
  int k;
  double u;
  GetRNGstate();
  u = runif(0.0,1.0);
  PutRNGstate();
  u = u*(veclen-1);
  k = (int) u;
  return k;
}
/* generate uniform random baseline antibody level */
double rndunif (double *basel){
  double u;
  GetRNGstate();
  u = runif(basel[0],basel[1]);
  PutRNGstate();
  return u;
}
/* generate lognormal random baseline antibody level */
double rndlnorm (double *basel){
  double fac, rsq, v1, v2;
  do{
    GetRNGstate();
    v1 = 2.0 * runif(0.0,1.0) - 1.0; /* Sample two random numbers in the  */
    PutRNGstate();
    GetRNGstate();
    v2 = 2.0 * runif(0.0,1.0) - 1.0; /* square (-1,1) in either direction */
    PutRNGstate();
    rsq = v1 * v1 + v2 * v2;         /* and check if they are in the unit */
  } while (rsq >= 1.0 || rsq == 0.0); /* circle. If not, sample again.    */
  fac = sqrt(-2.0 * log(rsq)/rsq);    /* Box-Muller transformation.       */
  return exp(basel[0] + basel[1] * v1 * fac);
}

/* This is the original routine for calculating antibody levels at age of  */
/* sampling, with infections occurring as a Poisson process with rate      */
/* lambda                                                                  */
double ymemry (double age, double lambda,
               double *iv, double *pv, double *bsl){
  double tau, arst, basl[2], ynoise;
  basl[0] = iv[0]; basl[1] = iv[1];
  ynoise = rndlnorm(bsl); /* need this only once */
  tau = tinterv(lambda); /* age at first infection */
  if(age < tau) return ynoise; /* no infection */
  if(age*lambda > MAXINF) tau = age - MAXINF/lambda; /* < MAXINF infections */
  arst = age - tau; /* remaining time */
  tau = tinterv(lambda); /* time to next infection */
  if(arst < tau) return ab(arst,basl,pv)+ynoise; /* 1 inf */
  while(arst > tau){ /* is there time for another infection?*/
    arst = arst - tau; /* remaining time after next infection */
    basl[0] = ab(tau,basl,pv); /* baseline for current infection */
    tau = tinterv(lambda); /* time to next infection */
  }
  return ab(arst,basl,pv)+ynoise;
}

/* modified function for generating intervals, accounting for vaccination   */
/* (as infection) at age avacc                                              */
double vinterv (double ainf, double avacc, double lambda){
  double tau;
  tau = tinterv(lambda);
  if(ainf + tau > avacc && ainf < avacc) return(avacc - ainf);
  return(tau);
}

/* Modified routine for calculating antibody levels at age of sampling,     */
/* Poisson process infections with rate lambda and vaccination at age avacc */
double yvmemry (double age, double avacc, double lambda,
                double *iv, double *pv, double *bsl){
  double tau, arst, ainf, basl[2], ynoise;
  arst = age; ainf = 0;
  basl[0] = iv[0]; basl[1] = iv[1];
  ynoise = rndlnorm(bsl); /* need this only once */
  tau = vinterv(ainf,avacc,lambda); /* age at first infection */
  if(age < tau) return ynoise; /* no infection */
  if(age*lambda > MAXINF) tau = age - MAXINF/lambda; /* < MAXINF infections */
  arst = age - tau; /* remaining time */
  ainf = ainf + tau;
  tau = vinterv(ainf,avacc,lambda); /* time to next infection */
  if(arst < tau) return ab(arst,basl,pv)+ynoise; /* 1 inf */
  while(arst > tau){ /* is there time for another infection?*/
    arst = arst - tau; /* remaining time after next infection */
    ainf = ainf + tau;
    basl[0] = ab(tau,basl,pv); /* baseline for current infection */
    tau = vinterv(ainf,avacc,lambda); /* time to next infection */
  }
  return ab(arst,basl,pv)+ynoise;
}
/* Variant ignoring carry over via y0                                       */
double yvrenew (double age, double avacc, double lambda,
                double *iv, double *pv, double *bsl){
  double tau, arst, ainf, basl[2], ynoise;
  arst = age; ainf = 0;
  basl[0] = iv[0]; basl[1] = iv[1];
  ynoise = rndlnorm(bsl); /* need this only once */
  tau = vinterv(ainf,avacc,lambda); /* age at first infection */
  if(age < tau) return ynoise; /* no infection */
  if(age*lambda > MAXINF) tau = age - MAXINF/lambda; /* < MAXINF infections */
  arst = age - tau; /* remaining time */
  ainf = ainf + tau;
  tau = vinterv(ainf,avacc,lambda); /* time to next infection */
  if(arst < tau) return ab(arst,basl,pv)+ynoise; /* 1 inf */
  while(arst > tau){ /* is there time for another infection?*/
    arst = arst - tau; /* remaining time after next infection */
    ainf = ainf + tau;
    basl[0] = ab(tau,basl,pv); /* baseline for current infection */
    tau = vinterv(ainf,avacc,lambda); /* time to next infection */
  }
  return ab(arst,iv,pv)+ynoise;
}

/* Calculate antibody levels at age of sampling, with intervention at date  */
/* tau_interv. Poisson process infections with rate lambda1 before and rate */
/* lambda2 after intervention                                               */
double yintervmemry (double age, double lambda1, double tau_interv,
		     double lambda2, double *iv, double *pv, double *bsl){
  double lambda, tau, basl[2], ynoise, ainf, age_interv;
  basl[0] = iv[0]; basl[1] = iv[1];
  ynoise = rndlnorm(bsl);                            /* need this only once */
  age_interv = age - tau_interv;                     /* age at intervention */
  if(age_interv < 0) age_interv = 0;           /* if before birth: set to 0 */
  lambda = lambda1;                        /* always start pre-intervention */
  if(age_interv == 0) lambda = lambda2; /* unless intervention before birth */
  tau = tinterv(lambda);                            /* age at 1st infection */
  if(tau > age_interv > 0){            /* intervention within 1st interval? */
    lambda = lambda2;
    tau = age_interv + tinterv(lambda);        /* remains post-intervention */
  }
  if(age < tau) return ynoise;                              /* no infection */
  ainf = tau;                              /* age of  most recent infection */
  tau = tinterv(lambda);                          /* time to next infection */
  if(ainf + tau > age_interv > ainf){  /* intervention within 2nd interval? */
    lambda = lambda2;
    tau = age_interv -ainf + tinterv(lambda);  /* remains post intervention */
  }
  if(ainf + tau > age) return ab(age-ainf,basl,pv)+ynoise;   /* 1 infection */
  while(ainf + tau < age){                     /* time of sampling reached? */
    ainf = ainf + tau;            /* remaining time after current infection */
    basl[0] = ab(tau,basl,pv);            /* baseline for current infection */
    tau = tinterv(lambda);                        /* time to next infection */
    if(ainf + tau > age_interv > ainf){ /* intervention within new interval?*/
      lambda = lambda2;
      tau = age_interv -ainf + tinterv(lambda);/* remains post intervention */
    }
  }
  return ab(age-ainf,basl,pv)+ynoise;    /* remaining from most recent inf. */
}

/* Generate antibody level for a person sampled at a given age   */
/* Infection history is in tau[0],tau[1],tau[2],...,tau[ninf-1]] */
double ytaumemry (double age, int ninf, double *tau,
                  double *iv, double *pv, double *bsl){
  int kinf = 1;
  double basl[2], ynoise;
  basl[0] = iv[0]; basl[1] = iv[1];
  ynoise = rndlnorm(bsl); /* need this only once */
  if(ninf == 0) return(ynoise); /* no infection */
  if(ninf == 1) return(ab(age-tau[0],basl,pv)+ynoise); /* one infection */
  while(kinf < ninf){
    basl[0] = ab(tau[kinf]-tau[kinf-1],basl,pv); /* baseline current inf  */
    kinf = kinf + 1;
  }
  kinf = kinf - 1;
  return(ab(age-tau[kinf],basl,pv)+ynoise);
}

/* From here deviate metrics for repeated infections in       */
/* subjects sampled at a given age with carried over y0       */
/* Note that yobs must be sorted!! (and ages accordingly)     */
/* Kolmogorov-Smirnow deviate                                 */
double drndksa_memry (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvmemry(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = ksdevtwo(logyobs,nobs,logysim,nsim);
  return dev;
}

/* Kolmogorov-Smirnow probability                             */
double drndksap_memry (double *logyobs, int nobs, int nsim, double *age,
                       double avacc, double lambda, int nmc, double *y0,
                       double *b0, double *mu0, double *mu1, double *c,
                       double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double prb, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvmemry(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  prb = ksprobtwo(logyobs,nobs,logysim,nsim);
  return prb;
}

/* Kolmogorov-Smirnow probability for intervention scenario    */
double drndksapinterv (double *logyobs, int nobs, int nsim, double *age,
                       double lambda1, double tau_interv, double lambda2,
                       int nmc, double *y0, double *b0,
                       double *mu0, double *mu1, double *c, double *alpha,
                       double *shape, double *bsl){
  int kmc, ksim;
  double prb, ivmc[2], pvmc[5], logysim[nsim];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yintervmemry(age[ksim],lambda1,tau_interv,lambda2,
                                     ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  prb = ksprobtwo(logyobs,nobs,logysim,nsim);
  return prb;
}

/* Kullback-Leibler divergence                                */
double drndklda_memry(double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvmemry(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = kldivtwo(logyobs,nobs,logysim,nsim);
  return dev;
}

/* Anderson-Darling deviance                                */
double drndada_memry (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvmemry(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = adstattwo(logyobs,nobs,logysim,nsim);
  return dev;
}

/* From here deviate metrics for repeated infections in       */
/* subjects sampled at a given age ignoring y0 (renew)        */
/* Note that yobs must be sorted!! (and ages accordingly)     */
/* Kolmogorov-Smirnow deviate                                 */
double drndksa_renew (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvrenew(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = ksdevtwo(logyobs,nobs,logysim,nsim);
  return dev;
}

/* Kolmogorov-Smirnow probability                             */
double drndksap_renew (double *logyobs, int nobs, int nsim, double *age,
                       double avacc, double lambda, int nmc, double *y0,
                       double *b0, double *mu0, double *mu1, double *c,
                       double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double prb, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvrenew(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  prb = ksprobtwo(logyobs,nobs,logysim,nsim);
  return prb;
}

/* Kullback-Leibler divergence                                */
double drndklda_renew (double *logyobs, int nobs, int nsim, double *age,
                       double avacc, double lambda, int nmc, double *y0,
                       double *b0, double *mu0, double *mu1, double *c,
                       double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvrenew(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = kldivtwo(logyobs,nobs,logysim,nsim);
  return dev;
}

/* Anderson-Darling deviance                                */
double drndada_renew (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvrenew(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = adstattwo(logyobs,nobs,logysim,nsim);
  return dev;
}

/* Alternative set of metrics with sim and obs swapped */
double drndksa_swap (double *logyobs, int nobs, int nsim, double *age,
                     double avacc, double lambda, int nmc, double *y0,
                     double *b0, double *mu0, double *mu1, double *c,
                     double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvmemry(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = ksdevtwo(logysim,nsim,logyobs,nobs);
  return dev;
}

/* Kolmogorov-Smirnow probability                             */
double drndksap_swap (double *logyobs, int nobs, int nsim, double *age,
                      double avacc, double lambda, int nmc, double *y0,
                      double *b0, double *mu0, double *mu1, double *c,
                      double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double prb, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvmemry(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  prb = ksprobtwo(logysim,nsim,logyobs,nobs);
  return prb;
}

/* Kullback-Leibler divergence                                */
double drndklda_swap(double *logyobs, int nobs, int nsim, double *age,
                     double avacc, double lambda, int nmc, double *y0,
                     double *b0, double *mu0, double *mu1, double *c,
                     double *alpha, double *shape, double *bsl){
  int ninf, kmc, ksim;
  double dev, ivmc[2], pvmc[5], logysim[nsim], tau[MAXINF];
  for(ksim=0; ksim < nsim; ksim++){
    kmc = rndind(nmc);
    ivmc[0] = y0[kmc]; ivmc[1] = b0[kmc];
    pvmc[0] = mu0[kmc]; pvmc[1] = mu1[kmc]; pvmc[2] = c[kmc];
    pvmc[3] = alpha[kmc]; pvmc[4] = shape[kmc];
    logysim[ksim] = log(yvmemry(age[ksim],avacc,lambda,ivmc,pvmc,bsl));
  }
  sort(logysim,nsim);
  dev = kldivtwo(logysim,nsim,logyobs,nobs);
  return dev;
}
