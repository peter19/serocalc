# serocalc

C and R code for calculating seroincidences.

C/serocalc.c
likelihood calculations for seroincidence estimation as in Teunis and
van Eijkeren (2020)

C/edfstat.c
statistics for comparing empirical distribution functions (EDFs)

C/simul.c
code using the within host model of de Graaf et al (2014) to simulate
cross-sectional population samples of serum antibody concentrations.

C/simulgener.c
adaptations to use different kinetic parameters for each infection episode

R/serocalc.r
function definitions for use in R

R/simul.r
function definitions for use in R

R/abc.r
functions for approximate Bayesian computation in R

R/simfuncgener.r
additional functions for seroincidence estimation by infection episode

R/simfunc1.r
R/simfuncm.r
functions to call for ABC methods (simfunc1.r for single kinetic
parameter set, simfuncm.r for kinetic parameters by infection episode)

References

Teunis and van Eijkeren (2020): Estimation of seroconversion rates for
infectious diseases: effects of age and noise.
Statistics in Medicine 39: 2799-2814. https://doi.org/10.1002/sim.8578

de Graaf et al (2014): A two-phase within-host model for immune response
and its application to serological profiles of pertussis.
Epidemics 9: 1-7. https://doi.org/10.1016/j.epidem.2014.08.002

Teunis et al (2016): Linking the seroresponse to infection to
within-host heterogeneity in antibody production.
Epidemics 16: 33-39. https://doi.org/10.1016/j.epidem.2016.04.001

Teunis et al (2023): Estimating seroconversion rates accounting for
repeated infections by approximate Bayesian computation.
Statistics in Medicine 42(28): 5160-5188. https://doi.org/10.1002/sim.9906
