# This software is released under the GNU AGPL v3.0 or later
# see agpl-3.0.txt

yvgener  <- function(age,avacc,lambda,initvec,parvec,basevec,ginf){
  res <- 0; age <- as.double(age); avacc <- as.double(avacc);
  lambda <- as.double(lambda); iv <- as.double(initvec);
  pv <- as.double(parvec); bsl <- as.double(basevec); ginf <- as.integer(ginf);
  yx <- .C("yvgenerr",res=as.double(res),age=age,avacc=avacc,
              lambda=lambda,iv=iv,pv=pv,bsl=bsl,ginf=ginf);
  return(yx$res);
}

ytaugener  <- function(age,ninf,tau,initvec,parvec,basevec,ginf){
  res <- 0; age <- as.double(age); ninf <- as.integer(ninf);
  tau <- as.double(tau); iv <- as.double(initvec);
  pv <- as.double(parvec); bsl <- as.double(basevec); ginf <- as.integer(ginf);
  yx <- .C("ytaugenerr",res=as.double(res),age=age,ninf=ninf,
              tau=tau,iv=iv,pv=pv,bsl=bsl,ginf);
  return(yx$res);
}

devsimksagener <- function(logyobs,nsim,age,avacc,lambda,
                           y0,b0,mu0,mu1,c,alpha,shape,bsvec,ginf){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); ginf <- as.integer(ginf); res <- 0.0;
  devs <- .C("drndksagenerr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl,ginf=ginf);
  return(devs$res);
}

probsimksagener <- function(logyobs,nsim,age,avacc,lambda,
                            y0,b0,mu0,mu1,c,alpha,shape,bsvec,ginf){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); ginf <- as.integer(ginf); res <- 0.0;
  devs <- .C("drndksapgenerr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl,ginf=ginf);
  return(devs$res);
}
