# This software is released under the GNU AGPL v3.0 or later
# see agpl-3.0.txt

prior <- function(par){
  pr <- 1;
  for(k in 1:length(par))
    pr <- pr*dnorm(par[k],mu.pr[k],sd.pr[k]);
  return(pr);
}

gencand <- function(par){
  candpar <- par;
  for(k in 1:length(par))
    candpar[k] <- candpar[k]+rnorm(1,0,par.sd[k]);
  return(candpar);
}

post <- function(par){
    return(prior(par)*amplif*kolmprob(par));
    # return(kolmprob(par));
}

btest <- function(){
  newpar <- gencand(oldpar);
  newpost <- post(newpar);
  ratio <- newpost/oldpost;
  # cat("\n",ratio," ",newpost,"\n")
  if(ratio >= runif(1,0,1)){
    oldpost <<- newpost;
    oldpar <<- newpar;
    acc <<- acc + 1;
  }
}

inc <- function(){
  mc.post[[length(mc.post)+1]] <<- oldpost;
  mc.par[[length(mc.par)+1]] <<- oldpar;
}

gentimes <- function(burnin=FALSE,n.post,n.thin){
  perc <- progress(burnin,0,n.post,0);
  for(k.iter  in  1:n.post){
    for(k.thin in 1:n.thin){
      btest();
    }
    inc();
    perc <- progress(burnin,k.iter,n.post,perc);
  }
}

progress <- function(burnin,j,r,oldperc){
  newperc <- round(100*j/r);
  if(newperc - oldperc >= 2){
    progbar(burnin,j,r);
    oldperc <- newperc;
  }
  return(oldperc);
}

progbar <- function(burnin,j,r){
  prgri <- round(100*j/r);
  star <- round(50*j/r); spce <- 50 - star;
  bsp <- ifelse(prgri<10,2,3);
  if(j!=0) cat(rep("\b",52+bsp),sep="");
  cat("|",sep="");
  if(burnin) cat(rep("+",star),sep="");
  if(!burnin) cat(rep("*",star),sep="");
  cat(rep(" ",spce),sep="");
  cat("|",sep="");
  cat(prgri,"%",sep="");
  if(prgri==100) cat("\n");
}

delistpost <-function(mcp) return(unlist(mc.post));
delistpar <- function(mcp) return(t(matrix(unlist(mc.par),nrow=length(mu.pr))));
