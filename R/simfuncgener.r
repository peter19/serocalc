# This software is released under the GNU AGPL v3.0 or later
# see agpl-3.0.txt

# simulation of cross-sectional serum antibody sample
source(paste(basepath,"R/simulgener.r",sep=""));

mkabsmplgener <- function(nsmp,avacc,lambda,asmp,nmc,ktst,bv,ginf){
  ysmp <- rep(NA,nsmp);
  for(ksmp in 1:nsmp){
    kmc <- sample.int(nmc,size=1);
    ysmp[ksmp] <- yvgener(day2yr*asmp[ksmp],avacc,lambda,
                          initp[ktst,,1,kmc],as.vector(kinep[ktst,,,kmc]),
                          bv,ginf);
  }
  ysmp[ysmp > 1e6] <- 1e6;
  return(ysmp);
}

# simulation-based fit functions

devksagener <- function(avacc,lambda,ysmp,aks,bv,ktst,ginf,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimksagener(log(ysmp),ny,aks,avacc,lambda,
      initp[ktst,1,1,],initp[ktst,2,1,],as.vector(kinep[ktst,1,,]),
      as.vector(kinep[ktst,2,,]),as.vector(kinep[ktst,3,,]),
      as.vector(kinep[ktst,4,,]),as.vector(kinep[ktst,5,,]),bv,ginf);
  return(kd/navr);
}

probksagener <- function(avacc,lambda,ysmp,aks,bv,ktst,ginf,navr){
  ny <- length(aks); kp <- 0;
  for(k in 1:navr)
    kp <- kp + probsimksagener(log(ysmp),ny,aks,avacc,lambda,
      initp[ktst,1,1,],initp[ktst,2,1,],as.vector(kinep[ktst,1,,]),
      as.vector(kinep[ktst,2,,]),as.vector(kinep[ktst,3,,]),
      as.vector(kinep[ktst,4,,]),as.vector(kinep[ktst,5,,]),bv,ginf);
  return(kp/navr);
}
