# This software is released under the GNU AGPL v3.0 or later
# see agpl-3.0.txt

# Utility functions: interface with C lib serocalc.so
# basepath <- "~/stat/seroincidence/simulation/generic/";
dyn.load(paste(basepath,"C/serocalc.so",sep=""));

fdev <- function(log.lambda,csdata,lnpars,cond)
{
  res <- 0; lambda <- as.double(exp(log.lambda));
  y <- as.double(csdata$y); a <- as.double(csdata$a);
  nsubj <- as.integer(length(y));
  y1 <- as.double(lnpars$y1); alpha <- as.double(lnpars$alpha);
  d <- as.double(lnpars$d); nmc <- as.integer(length(y1));
  step <- as.double(max(y1)/100); # hack for numerical integrations
  nu <- as.double(cond$nu); eps <- as.double(cond$eps);
  y.low <- as.double(cond$y.low); y.high <- as.double(cond$y.high);
  llpp <- .C("negloglik",res=as.double(res),
             lambda=lambda,y=y,a=a,nsubj=nsubj,
             nu=nu,eps=eps,step=step,y.low=y.low,y.high=y.high,
             y1=y1,alpha=alpha,d=d,nmc=nmc);
  return(llpp$res);
}

fdevm <- function(log.lambda,ydata,adata,lnpars,cond)
{
  res <- 0; lambda <- as.double(exp(log.lambda));
  y <- as.double(ydata); a <- as.double(adata);
  nsubj <- as.integer(length(y));
  y1 <- as.double(lnpars[1,]); alpha <- as.double(lnpars[2,]);
  d <- as.double(lnpars[3,]); nmc <- as.integer(length(y1));
  step <- as.double(max(y1)/100); # hack for numerical integrations
  nu <- as.double(cond[1]); eps <- as.double(cond[2]);
  y.low <- as.double(cond[3]); y.high <- as.double(cond[4]);
  llpp <- .C("negloglik",res=as.double(res),
             lambda=lambda,y=y,a=a,nsubj=nsubj,
             nu=nu,eps=eps,step=step,y.low=y.low,y.high=y.high,
             y1=y1,alpha=alpha,d=d,nmc=nmc);
  return(llpp$res);
}

fdevmulti <- function(log.lambda,ydatam,adata,lnpm,condm)
{
  res <- 0; ntst <- dim(ydatam)[1];
  if(ntst==1) return(fdevm(log.lambda,ydatam[1,],adata,lnpm,condm));
  for(ktst in 1:ntst)
    res <- res+fdevm(log.lambda,ydatam[ktst,],adata,lnpm[ktst,,],condm[ktst,]);
  return(res);
}
