# This software is released under the GNU AGPL v3.0 or later
# see agpl-3.0.txt

# basepath <- "~/stat/seroincidence/simulation/generic/";
# the serocalculator (v3) functions

source(paste(basepath,"R/serocalc.r",sep="")); # also loads serocalc.so

scalcv3 <- function(lam.init,asmp,ysmp,lnpars,cond){
  llam.init <- log(lam.init);
  max.step <- (log(lam.init*10)-log(lam.init/10))/4;

  objfunc <- function(llam){
    return(fdevmulti(llam,ysmp,asmp,lnpars,cond));
  }
  fit <- nlm(objfunc,llam.init,hessian=TRUE,print.level=0,stepmax=max.step);
  llam.est = c(fit$estimate,
               fit$estimate + qnorm(c(0.05))*sqrt(1/fit$hessian),
               fit$estimate + qnorm(c(0.95))*sqrt(1/fit$hessian));
  return(exp(llam.est));
}

# simulation of cross-sectional serum antibody sample

source(paste(basepath,"R/simul.r",sep="")); # also loads simul.so

mkagsmpl <- function(nsmp,minim,maxim){
  return(runif(n=nsmp,min=minim,max=maxim));
}
mkabsmplmemry <- function(nsmp,avacc,lambda,asmp,nmc,ktst,bv){
  ysmp <- rep(NA,nsmp);
  for(ksmp in 1:nsmp){
    kmc <- sample.int(nmc,size=1);
    ysmp[ksmp] <- yvmemry(day2yr*asmp[ksmp],avacc,lambda,
                          initp1[ktst,,kmc],kinep1[ktst,,kmc],bv);
  }
  ysmp[ysmp > 1e6] <- 1e6;
  return(ysmp);
}

mkabsmplinterv <- function(nsmp,lambda1,tau_interv,lambda2,asmp,nmc,ktst,bv){
  ysmp <- rep(NA,nsmp);
  for(ksmp in 1:nsmp){
    kmc <- sample.int(nmc,size=1);
    ysmp[ksmp] <- yintervmemry(day2yr*asmp[ksmp],lambda1,tau_interv,lambda2,
                          initp1[ktst,,kmc],kinep1[ktst,,kmc],bv);
  }
  ysmp[ysmp > 1e6] <- 1e6;
  return(ysmp);
}

mkabsmplrenew <- function(nsmp,avacc,lambda,asmp,nmc,ktst,bv){
  ysmp <- rep(NA,nsmp);
  for(ksmp in 1:nsmp){
    kmc <- sample.int(nmc,size=1);
    ysmp[ksmp] <- yvrenew(day2yr*asmp[ksmp],avacc,lambda,
                          initp1[ktst,,kmc],kinep1[ktst,,kmc],bv);
  }
  return(ysmp);
}

# simulation-based fit functions

ksdata <- function(ysmp,asmp,nfac){
  srtd <- order(ysmp);
  nsmp <- length(ysmp);
  ny <- nsmp*nfac; # nr simulated y = nr cs sample (think of ages!)
  if(nfac==1) ay <- asmp[srtd];
  if(nfac!=1) ay <- sample(asmp,size=ny,replace=TRUE);
  return(list(y=ysmp[srtd],a=ay)); # can be unequal length!
}

devksamemry <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimksamemry(log(ysmp),ny,aks,avacc,lambda,
                              initp1[ktst,1,],initp1[ktst,2,],
                              kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                              kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

devkldamemry <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimkldamemry(log(ysmp),ny,aks,avacc,lambda,
                               initp1[ktst,1,],initp1[ktst,2,],
                               kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                               kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

probksamemry <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kp <- 0;
  for(k in 1:navr)
    kp <- kp + probsimksamemry(log(ysmp),ny,aks,avacc,lambda,
                         initp1[ktst,1,],initp1[ktst,2,],
                         kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                         kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kp/navr);
}

probksainterv <- function(lambda1,tau_interv,lambda2,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kp <- 0;
  for(k in 1:navr)
    kp <- kp + probsimksainterv(log(ysmp),ny,aks,lambda1,tau_interv,lambda2,
                         initp1[ktst,1,],initp1[ktst,2,],
                         kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                         kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kp/navr);
}

devadamemry <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimadamemry(log(ysmp),ny,aks,avacc,lambda,
                              initp1[ktst,1,],initp1[ktst,2,],
                              kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                              kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

devksarenew <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimksarenew(log(ysmp),ny,aks,avacc,lambda,
                              initp1[ktst,1,],initp1[ktst,2,],
                              kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                              kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

devkldarenew <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimkldarenew(log(ysmp),ny,aks,avacc,lambda,
                               initp1[ktst,1,],initp1[ktst,2,],
                               kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                               kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

probksarenew <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kp <- 0;
  for(k in 1:navr)
    kp <- kp + probsimksarenew(log(ysmp),ny,aks,avacc,lambda,
                         initp1[ktst,1,],initp1[ktst,2,],
                         kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                         kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kp/navr);
}

devadarenew <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimadarenew(log(ysmp),ny,aks,avacc,lambda,
                              initp1[ktst,1,],initp1[ktst,2,],
                              kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                              kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

devksaswap <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimksaswap(log(ysmp),ny,aks,avacc,lambda,
                              initp1[ktst,1,],initp1[ktst,2,],
                              kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                              kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

devkldaswap <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kd <- 0;
  for(k in 1:navr)
    kd <- kd + devsimkldaswap(log(ysmp),ny,aks,avacc,lambda,
                               initp1[ktst,1,],initp1[ktst,2,],
                               kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                               kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kd/navr);
}

probksaswap <- function(avacc,lambda,ysmp,aks,bv,ktst,navr){
  ny <- length(aks); kp <- 0;
  for(k in 1:navr)
    kp <- kp + probsimksaswap(log(ysmp),ny,aks,avacc,lambda,
                         initp1[ktst,1,],initp1[ktst,2,],
                         kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                         kinep1[ktst,4,],kinep1[ktst,5,],bv);
  return(kp/navr);
}

ksdevmemry <- function(avacc,llam,ysmp,aks,bv,ktst,navr){
  kmdev <- rep(NA,length(llam));
  for(klam in 1:length(llam))
    kmdev[klam] <- devksamemry(avacc,exp(llam[klam]),ysmp,aks,bv,ktst,navr);
  return(kmdev);
}

ksprbmemry <- function(avacc,llam.st,llam.stp,ysmp,aks,bv,ktst,navr){
  llam <- llam.st;
  ksp <- probksamemry(avacc,exp(llam),ysmp,aks,bv,ktst,navr);
  llam2 <- llam.st; ksprb <- ksp;
  while(ksp >= 1e-10 & llam - llam.st < 2.5){
    llam <- llam + llam.stp;
    ksp <- probksamemry(avacc,exp(llam),ysmp,aks,bv,ktst,navr);
    llam2 <- c(llam2,llam);
    ksprb <- c(ksprb,ksp);
  }
  llam <- llam.st; ksp <- ksprb[1];
  while(ksp >= 1e-10 & llam.st-llam < 2.5){
    llam <- llam - llam.stp;
    ksp <- probksamemry(avacc,exp(llam),ysmp,aks,bv,ktst,navr);
    llam2 <- c(llam,llam2);
    ksprb <- c(ksp,ksprb);
  }
  return(data.frame(llam=llam2,p=ksprb));
}

kslogprbmemry <- function(avacc,llam.st,llam.stp,ysmp,aks,bv,ktst,navr){
  llam <- llam.st;
  ksp <- probksamemry(avacc,exp(llam),ysmp,aks,bv,ktst,navr);
  llam2 <- llam.st; kslprb <- log(ksp); kslp <- kslprb;
  while(kslprb[1]-kslp < 8 & llam - llam.st < 2.5){
    llam <- llam + llam.stp;
    ksp <- probksamemry(avacc,exp(llam),ysmp,aks,bv,ktst,navr);
    kslp <- log(ksp);
    llam2 <- c(llam2,llam);
    kslprb <- c(kslprb,kslp);
  }
  llam <- llam.st; kslp <- kslprb[1];
  while(kslprb[1]-kslp < 8 & llam.st-llam < 2.5){
    llam <- llam - llam.stp;
    ksp <- probksamemry(avacc,exp(llam),ysmp,aks,bv,ktst,navr);
    kslp <- log(ksp);
    llam2 <- c(llam,llam2);
    kslprb <- c(kslp,kslprb);
  }
  return(data.frame(llam=llam2,p=exp(kslprb)));
}

findinterv <- function(y,yref){
  n.ref <- length(yref);
  if(is.na(y)) return(0.5/(n.ref+2));
  if(y <= yref[1]) return(0.5/(n.ref+2));
  if(y >= yref[n.ref]) return((n.ref+0.5)/(n.ref+2))
  kl <- rev(which(yref < y))[1]; # lower
  kh <- which(yref > y)[1];      # upper
  r <- (y - yref[kl])/(yref[kh]-yref[kl]);
  return((kl+r)/(n.ref+2));
}

qq <- function(y,yref){
  ym.dist <- rep(NA,length(y));
  for(k in 1:length(y)) ym.dist[k] <- findinterv(y[k],yref);
  return(ym.dist);
}

findmxdev <- function(y,yref){
  n.y <- length(y);
  ym.dist <- rep(NA,length(y));
  mxdev <- 0;
  for(k in 1:length(y)){
    ym.dist[k] <- findinterv(y[k],yref);
    dev <- abs(ym.dist[k]-(k/n.y));
    if(mxdev < dev) mxdev <- dev;
  }
  return(mxdev);
}

findpmdev <- function(y,yref){
  n.y <- length(y);
  ym.dist <- rep(NA,length(y));
  mxdevpls <- 0; mxdevmin <- 0;
  for(k in 1:length(y)){
    ym.dist[k] <- findinterv(y[k],yref);
    devpls <- k/n.y - ym.dist[k];
    #devmin <- ym.dist[k]-((k-1)/n.y);
    devmin <- ym.dist[k]-((k)/n.y);
    if(mxdevpls < devpls) mxdevpls <- devpls;
    if(mxdevmin < devmin) mxdevmin <- devmin;
  }
  return(mxdevpls + mxdevmin);
}

ksdev.memry.r <- function(avacc,llam,ysmp,aks,bv,ktst){
  mx1dev <- 1000;
  yqq <- array(NA,dim=c(length(llam),length(aks)));
  kmdev1 <- rep(NA,length(llam));
  for(k.lam in 1:length(llam)){
    ym <- mkabsmplmemry(length(aks),aks,avacc,exp(llam[k.lam]),nmc,ktst,bv);
    ym.c <- log(sort(ym));
    yref.c <- log(sort(ysmp));
    yqq[k.lam,] <- qq(ym.c,yref.c);
    kmdev1[k.lam] <- findmxdev(ym.c,yref.c);
    if(kmdev1[k.lam] < mx1dev){
      mx1dev <- kmdev1[k.lam];
      ymx1.c <- ym.c;
    }
  }
  return(list(kmdev1=kmdev1,ymx1=ymx1.c,yqq=yqq));
}

estimdev.memry <- function(avacc,lam.init,bv.init,ydata,adata,
                           n.fac,ktst,n.aver){
  ksd <- ksdata(ysmp=ydata,asmp=adata,nfac=n.fac); # prepare data
  ny <- length(ksd$y); rpt1 <- n.aver;
  kmdev <- function(par){
    kd <- 0;
    for(rp in 1:rpt1)
      kd <- kd + devsimksamemry(log(ksd$y),ny,ksd$a*day2yr,avacc,exp(par[1]),
                                initp1[ktst,1,],initp1[ktst,2,],
                                kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                                kinep1[ktst,4,],kinep1[ktst,5,],
                                c(par[2],exp(par[3])));
    return(kd/rpt1);
  }
  # res1 <- nlm(f=kmdev,p=c(log(lam.init),bv.init[1],log(bv.init[2])));
  # lam.est <- exp(res1$estimate[1])*day2yr;
  # bv.est <- c(res1$estimate[2],exp(res1$estimate[3]));
  # cat("lambda=",llam.est," bv=(",bv.est[1],",",bv.est[2],")\n");
  # return(c(lam.est,bv.est));
  res1 <- optim(par=c(log(lam.init),bv.init[1],log(bv.init[2])),kmdev,
                method="Nelder-Mead",
                control=list(trace=0,fnscale=1,maxit=1000,reltol=1e-3));
  lam.est <- exp(res1$par[1])*day2yr;
  bv.est <- c(res1$par[2],exp(res1$par[3]));
  # cat("lambda=",lam.est," bv=(",bv.est[1],",",bv.est[2],")\n");
  return(c(lam.est,bv.est));
}

estimprb.memry <- function(avacc,lam.init,bv.init,ydata,adata,
                           n.fac,ktst,n.aver){
  ksd <- ksdata(ysmp=ydata,asmp=adata,nfac=n.fac); # prepare data
  ny <- length(ksd$y); rpt2 <- 2*n.aver;
  kmprb <- function(par){
    kp <- 0;
    for(rp in 1:(rpt2))
      kp <- kp + probsimksamemry(log(ksd$y),ny,ksd$a*day2yr,avacc,exp(par[1]),
                                 initp1[ktst,1,],initp1[ktst,2,],
                                 kinep1[ktst,1,],kinep1[ktst,2,],kinep1[ktst,3,],
                                 kinep1[ktst,4,],kinep1[ktst,5,],
                                 c(par[2],exp(par[3])));
    return(kp/(rpt2));
  }
  # res2 <- nlm(f=kmprb,p=c(log(lam.init),bv.init[1],log(bv.init[2])));
  # lam.est <- exp(res2$estimate[1])*day2yr;
  # bv.est <- c(res2$estimate[2],exp(res2$estimate[3]));
  # cat("lambda=",llam.est," bv=(",bv.est[1],",",bv.est[2],")\n");
  # return(c(lam.est,bv.est));
  res2 <- optim(par=c(log(lam.init),bv.init[1],log(bv.init[2])),kmprb,
                method="Nelder-Mead",
                control=list(trace=0,fnscale=-1,maxit=1000,reltol=1e-3));
  lam.est <- exp(res2$par[1])*day2yr;
  bv.est <- c(res2$par[2],exp(res2$par[3]));
  # cat("lambda=",lam.est," bv=(",bv.est[1],",",bv.est[2],")\n");
  return(c(lam.est,bv.est));
}

gridprb.memry <- function(avacc,lam.init,bvec,step,ydata,adata,
                          n.fac,ktst,n.aver){
  ksd <- ksdata(ysmp=ydata,asmp=adata,nfac=n.fac); # prepare data
  prb <- ksprbmemry(avacc,log(lam.init),llam.stp=step,ysmp=ksd$y,
                    aks=ksd$a*day2yr,bv=bvec,ktst=ktst,navr=n.aver);
  return(prb);
}

findmxprb <- function(prblst){
  # mx <- floor(median(which(prblst$p==max(prblst$p)))); # may be > 1
  mx <- which(prblst$p==max(prblst$p)); # may be > 1
  ci <- which(prblst$p >= 0.025); ci <- c(min(ci),max(ci));
  return(c(mean(prblst$llam[mx]),prblst$llam[ci[1]],prblst$llam[ci[2]],
           prblst$p[mx]));
}

