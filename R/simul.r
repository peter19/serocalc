# This software is released under the GNU AGPL v3.0 or later
# see agpl-3.0.txt

#  Utility functions: interface with C lib simul.so
#  basepath <- "~/stat/seroincidence/simulation/generic/";
dyn.load(paste(basepath,"C/simul.so",sep=""));

a <- function(initvec,parvec){
  res <- 0;
  iv <- as.double(initvec); pv <- as.double(parvec);
  ax <- .C("ar",res=as.double(res),iv=iv,pv=pv);
  return(ax$res);
}

b <- function(parvec){
  res <- 0;
  pv <- as.double(parvec);
  bx <- .C("br",res=as.double(res),pv=pv);
  return(bx$res);
}

t1 <- function(initvec,parvec){
  res <- 0;
  iv <- as.double(initvec); pv <- as.double(parvec);
  t1x <- .C("t1r",res=as.double(res),iv=iv,pv=pv);
  return(t1x$res);
}

y1 <- function(initvec,parvec){
  res <- 0;
  iv <- as.double(initvec); pv <- as.double(parvec);
  y1x <- .C("y1r",res=as.double(res),iv=iv,pv=pv);
  return(y1x$res);
}

ag <- function(t,initvec,parvec){
  res <- 0; t <- as.double(t);
  iv <- as.double(initvec); pv <- as.double(parvec);
  agx <- .C("agr",res=as.double(res),t=t,iv=iv,pv=pv);
  return(agx$res);
}

ab <- function(t,initvec,parvec){
  res <- 0; t <- as.double(t);
  iv <- as.double(initvec); pv <- as.double(parvec);
  abx <- .C("abr",res=as.double(res),t=t,iv=iv,pv=pv);
  return(abx$res);
}

symp <- function(initvec,parvec){
  res <- 0;
  iv <- as.double(initvec); pv <- as.double(parvec);
  sympx <- .C("sympr",res=as.integer(res),iv=iv,pv=pv);
  return(sympx$res);
}

tinterv <- function(lambda){
  res <-  0; lambda <- as.double(lambda);
  tintervx <- .C("tintervr",res=as.double(res),lambda=lambda);
  return(tintervx$res);
}

rndind <- function(sz){
  res <-  0; sz <- as.integer(sz);
  rndindx <- .C("rndindr",res=as.integer(res),size=sz);
  return(rndindx$res)
}

unifbasel <- function(bslvec){
  res <-  0; bslvec <- as.double(bslvec);
  baselx <- .C("rndunifr",res=as.double(res),basel=bslvec);
  return(baselx$res);
}

lnormbasel <- function(bslvec){
  res <-  0; bslvec <- as.double(bslvec);
  baselx <- .C("rndlnormr",res=as.double(res),basel=bslvec);
  return(baselx$res);
}

vinterv <- function(ainf,avacc,lambda){
  res <- 0; ainf  <- as.double(ainf); avacc <- as.double(avacc);
  lambda <- as.double(lambda);
  tx <- .C("vintervr",res=as.double(res),ainf=ainf,avacc=avacc,lambda=lambda);
  return(tx$res);
}

ymemry  <- function(age,lambda,initvec,parvec,basevec){
  res <- 0; age <- as.double(age);
  lambda <- as.double(lambda); iv <- as.double(initvec);
  pv <- as.double(parvec); bsl <- as.double(basevec);
  yx <- .C("ymemryr",res=as.double(res),age=age,
              lambda=lambda,iv=iv,pv=pv,bsl=bsl);
  return(yx$res);
}

yvmemry  <- function(age,avacc,lambda,initvec,parvec,basevec){
  res <- 0; age <- as.double(age); avacc <- as.double(avacc);
  lambda <- as.double(lambda); iv <- as.double(initvec);
  pv <- as.double(parvec); bsl <- as.double(basevec);
  yx <- .C("yvmemryr",res=as.double(res),age=age,avacc=avacc,
              lambda=lambda,iv=iv,pv=pv,bsl=bsl);
  return(yx$res);
}

yvrenew  <- function(age,avacc,lambda,initvec,parvec,basevec){
  res <- 0; age <- as.double(age); avacc <- as.double(avacc);
  lambda <- as.double(lambda); iv <- as.double(initvec);
  pv <- as.double(parvec); bsl <- as.double(basevec);
  yx <- .C("yvrenewr",res=as.double(res),age=age,avacc=avacc,
              lambda=lambda,iv=iv,pv=pv,bsl=bsl);
  return(yx$res);
}

yintervmemry  <- function(age,lambda1,tau_interv,lambda2,
                          initvec,parvec,basevec){
  res <- 0; age <- as.double(age); lambda1 <- as.double(lambda1);
  tau_interv <- as.double(tau_interv); lambda2 <- as.double(lambda2);
  iv <- as.double(initvec);
  pv <- as.double(parvec); bsl <- as.double(basevec);
  yx <- .C("yintervmemryr",res=as.double(res),age=age,lambda1=lambda1,
           tau_interv=tau_interv,lambda2=lambda2,iv=iv,pv=pv,bsl=bsl);
  return(yx$res);
}

ytaumemry  <- function(age,ninf,tau,initvec,parvec,basevec){
  res <- 0; age <- as.double(age); ninf <- as.integer(ninf);
  tau <- as.double(tau); iv <- as.double(initvec);
  pv <- as.double(parvec); bsl <- as.double(basevec);
  yx <- .C("ytaumemryr",res=as.double(res),age=age,ninf=ninf,
              tau=tau,iv=iv,pv=pv,bsl=bsl);
  return(yx$res);
}

devsimksamemry <- function(logyobs,nsim,age,avacc,lambda,
                           y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndksamemryr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

probsimksamemry <- function(logyobs,nsim,age,avacc,lambda,
                            y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndksapmemryr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

probsimksainterv <- function(logyobs,nsim,age,lambda1,tau_interv,lambda2,
                             y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  lambda1 <- as.double(lambda1); tau_interv <- as.double(tau_interv);
  lambda2 <- as.double(lambda2);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndksapintervr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,lambda1=lambda1,tau_interv=tau_interv,
             lambda2=lambda2,nmc=nmc,y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,
             alpha=alpha,shape=shape,bsl=bsl);
  return(devs$res);
}

devsimkldamemry <- function(logyobs,nsim,age,avacc,lambda,
                            y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndkldamemryr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

devsimadamemry <- function(logyobs,nsim,age,avacc,lambda,
                           y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndadamemryr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

devsimksarenew <- function(logyobs,nsim,age,avacc,lambda,
                           y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndksarenewr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

probsimksarenew <- function(logyobs,nsim,age,avacc,lambda,
                            y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndksaprenewr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

devsimkldarenew <- function(logyobs,nsim,age,avacc,lambda,
                            y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndkldarenewr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

devsimadarenew <- function(logyobs,nsim,age,avacc,lambda,
                           y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndadarenewr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

devsimksaswap <- function(logyobs,nsim,age,avacc,lambda,
                          y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndksaswapr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

probsimksaswap <- function(logyobs,nsim,age,avacc,lambda,
                           y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndksapswapr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

devsimkldaswap <- function(logyobs,nsim,age,avacc,lambda,
                           y0,b0,mu0,mu1,c,alpha,shape,bsvec){
  logyobs <- as.double(logyobs); nobs <- as.integer(length(logyobs));
  nsim <- as.integer(nsim); age <- as.double(age);
  avacc <- as.double(avacc); lambda <- as.double(lambda);
  nmc <- as.integer(length(y0)); y0 <- as.double(y0); b0 <- as.double(b0);
  mu0 <- as.double(mu0);  mu1 <- as.double(mu1);  c <- as.double(c);
  alpha <- as.double(alpha);  shape <- as.double(shape);
  bsl <- as.double(bsvec); res <- 0.0;
  devs <- .C("drndkldaswapr",res=as.double(res),logyobs=logyobs,nobs=nobs,
             nsim=nsim,age=age,avacc=avacc,lambda=lambda,nmc=nmc,
             y0=y0,b0=b0,mu0=mu0,mu1=mu1,c=c,alpha=alpha,shape=shape,
             bsl=bsl);
  return(devs$res);
}

kldivtwo <- function(dat1,dat2){
  d1 <- as.double(dat1); n1 <- length(dat1);
  d2 <- as.double(dat2); n2 <- length(dat2);
  res <- 0.0;
  ks2 <- .C("kldivtwor",res=as.double(res),d1,n1,d2,n2);
  return(ks2$res);
}
ksdevtwo <- function(dat1,dat2){
  d1 <- as.double(dat1); n1 <- length(dat1);
  d2 <- as.double(dat2); n2 <- length(dat2);
  res <- 0.0;
  ks2 <- .C("ksdevtwor",res=as.double(res),d1,n1,d2,n2);
  return(ks2$res);
}
ksprobtwo <- function(dat1,dat2){
  d1 <- as.double(dat1); n1 <- length(dat1);
  d2 <- as.double(dat2); n2 <- length(dat2);
  res <- 0.0;
  ks2 <- .C("ksprobtwor",res=as.double(res),d1,n1,d2,n2);
  return(ks2$res);
}
